#Source Path
SRC_PATH|src

#Entry point
MAIN_SRC|src\bob.c

#Include paths
INC_PATH|src\include\

#Compiled object files path
OBJ_PATH|bin\obj\

#Compiles executable path
EXE_PATH|bin\bob.exe

#Additional command line params
#AD_PARAM|/MD 

#vcvarsall.bat file location
VCVARSALL|"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"